import 'package:flutter/material.dart';
import 'package:newappday10/providers/cart.dart';
import 'package:newappday10/providers/products.dart';
import 'package:newappday10/screens/cart_screen.dart';
import 'package:newappday10/widgets/app_drawer.dart';
import 'package:newappday10/widgets/badge.dart';
import 'file:///C:/Users/AppVesto/IdeaProjects/newappday10/lib/providers/product.dart';
import 'package:newappday10/widgets/product_grid.dart';
import 'package:newappday10/widgets/product_item.dart';
import 'package:provider/provider.dart';

enum FiltersOptions {
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  bool _showOnlyFavorites = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My shop'),
          actions: <Widget>[
            PopupMenuButton(
              onSelected: (FiltersOptions selectedValue) {
                setState(() {
                  if (selectedValue == FiltersOptions.Favorites) {
                    _showOnlyFavorites = true;
                  } else {
                    _showOnlyFavorites = false;
                  }
                });
              },
              icon: Icon(
                Icons.more_vert,
              ),
              itemBuilder: (_) => [
                PopupMenuItem(
                  child: Text('Only Favorites'),
                  value: FiltersOptions.Favorites,
                ),
                PopupMenuItem(
                  child: Text('Show all'),
                  value: FiltersOptions.All,
                ),
              ],
            ),
            Consumer<Cart>(
              builder: (_, cartData, ch) => Badge(
                child: ch,
                value: cartData.itemCount.toString(),
              ),
              child: IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.of(context).pushNamed(CartScreen.routeName);
                },
              ),
            )
          ],
        ),
        drawer: AppDrawer(),
        body: ProductsGrid(_showOnlyFavorites));
  }
}
